import asyncio
import logging
import json
from concurrent.futures import ThreadPoolExecutor

from aiohttp import web
from ruamel.yaml import YAML
import gitlab

import gitlab_helpers


async def respond_to_hook(hook, app):
    """Convert a webhook to a corresponding gitlab object.

    Parameters
    ----------
    hook : json-like data
        Contents of the webhook.
    gitlab_api : Gitlab instance
        Should already be authenticated.
    """
    kind = hook['object_kind']
    gitlab_api = app['gitlab_api']
    executor = app['executor']
    print(kind)
    if kind == 'issue':
        issue = await app.loop.run_in_executor(
            executor,
            gitlab_helpers.hook_to_issue,
            hook,
            gitlab_api,
        )
        print(issue)


async def react_to_hook(request):
    try:
        data = await request.json()
    except json.decoder.JSONDecodeError:
        pass
    else:
        asyncio.ensure_future(
            respond_to_hook(data, app=request.app)
        )

    return web.Response()


async def init_gitlab(app):
    yaml = YAML()
    with open('config.yml') as f:
        config = yaml.load(f)
    gitlab_api = gitlab.Gitlab(
        config['gitlab_url'], private_token=config['token']
    )
    app['gitlab_api'] = gitlab_api


async def setup_executor(app):
    # Gitlab uses requests, which is thread-safe.
    app['executor'] = ThreadPoolExecutor()


async def shutdown_executor(app):
    app['executor'].shutdown()


app = web.Application()
app.add_routes([web.post('/', react_to_hook)])
app.on_startup.extend([init_gitlab, setup_executor])
app.on_cleanup.append(shutdown_executor)


if __name__ == "__main__":
    web.run_app(app)

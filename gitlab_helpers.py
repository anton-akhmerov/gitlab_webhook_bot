"""Helper functions for doing high level operations with gitlab.

Because python-gitlab is synchronous, all of these functions should be run in
an executor.
"""


def hook_to_issue(hook, gitlab_api):
    project = gitlab_api.projects.get(hook['project']['id'])
    issue = project.issues.get(hook['object_attributes']['iid'])
    return issue

# A gitlab organizational bot

It is supposed to do two things:
- Respond to webhooks
- Scan the repositories periodically

## Hacking

1. Requirements: `pip install aiohttp, aiohttp-devtools, python-gitlab`
2. Running the server locally (with automated restarts): `adev runserver gitlab_bot.py`
3. Tunneling webhooks: `ssh -R 0.0.0.0:XXXX:localhost:8000 tnw-tnY`
4. Point gitlab webhooks in your project (or system hooks) to `http://tnw-tnY.tudelft.net:XXXX`

## Security model:

The bot should run with elevated permissions (it is supposed to create/modify projects), push, etc.
Therefore it may not trust the payload of the webhook, instead relying on the gitlab API responses as the main source of truth.

## Design

For now the design is as follows:
- An aiohttp server
- Run by supervisor within a docker container
- Reading secret variables from a volume perhaps (not the best practice, might need revisiting)
